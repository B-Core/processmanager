const baseIndexConfig = {
  name: 'ProcessManager',
  input: 'src/index.js',
  output: {
    file: 'dist/index.js',
    format: 'es'
  }
};

const baseProcessConfig = {
  name: 'Process',
  input: 'src/models/process.js',
  output: {
    file: 'dist/models/process.js',
    format: 'es'
  }
};

export default [
  /* es module configuration */
  baseIndexConfig,
  /* umd module configuration */
  {
    ...baseIndexConfig,
    output: {
      file: 'dist/index.umd.js',
      format: 'umd'
    }
  },
  /* iife module configuration */
  {
    ...baseIndexConfig,
    output: {
      file: 'dist/index.iife.js',
      format: 'iife'
    }
  },
  /* cjs module configuration */
  {
    ...baseIndexConfig,
    output: {
      file: 'dist/index.cjs.js',
      format: 'cjs'
    }
  },
  /* es module configuration */
  baseProcessConfig,
  /* umd module configuration */
  {
    ...baseProcessConfig,
    output: {
      file: 'dist/models/process.umd.js',
      format: 'umd'
    }
  },
  /* iife module configuration */
  {
    ...baseProcessConfig,
    output: {
      file: 'dist/models/process.iife.js',
      format: 'iife'
    }
  },
  /* cjs module configuration */
  {
    ...baseProcessConfig,
    output: {
      file: 'dist/models/process.cjs.js',
      format: 'cjs'
    }
  },
];