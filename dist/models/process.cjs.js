'use strict';

var difference = (source, inputs) => {
  const flatArray = inputs.filter(i => (i !== null && typeof i !== 'undefined')).map(i => i instanceof Object ? JSON.stringify(i) : i);
  let diffs = source
  .filter(i => (i !== null && typeof i !== 'undefined'))
  .filter((i) => {
    const e = i instanceof Object ? JSON.stringify(i) : i;
    const index = flatArray.indexOf(e);
    if (index >= 0) flatArray.splice(index, 1);
    return !(index >= 0);
  });
  return diffs;
};

const nRegEx = new RegExp(/[\-\+]{0,1}\d+(\.\d+)?/, 'g');
const maxRegEx = new RegExp(/^>\d+(\.\d+)?$/);
const minRegEx = new RegExp(/^<\d+(\.\d+)?$/);
const rangeRegEx = new RegExp(/^\d+(\.\d+)?<>\d+(\.\d+)?$/);

var index = ({
  validate(input, schema, strict = false) {
    const keys = Object.keys(schema);
    const notVarKeys = keys.filter(k => !k.includes('?'));
    const inputKeys = Object.keys(input);
    const keyDifference = difference(notVarKeys, inputKeys);

    if (keyDifference.length > 0) return this.logError({
      input,
      source: keys,
      valid: false,
      message: `ValidationError: ${JSON.stringify(input)} missing keys [${keyDifference}]`
    });

    inputKeys.forEach(key => {
      const source = (schema[key] || schema[`?${key}`]);
      if (source) this.check(input[key], source);
      else if (!source && strict) return this.checkArray(inputKeys, keys);
    });

    return null;
  },
  check(input, source) {
    if (typeof source !== 'undefined' && source !== null) {
      if (source instanceof Function) return this.checkType(input, source);
      const numberTest = (maxRegEx.test(source) || minRegEx.test(source) || rangeRegEx.test(source)) || this.getType(source) === 'Number';
      const stringTest = typeof source === 'string' || source instanceof RegExp || this.getType(source) === 'String';
      const arrayTest = source instanceof Array;

      if (numberTest) {
        return this.checkNumber(input, source);
      } else if (stringTest) {
        return this.checkString(input, source);
      } else if (arrayTest) {
        const typeArray = source.filter(o => o instanceof Function);
        if (typeArray.length > 0 && typeArray.length === source.length) {
          let success = false;
          for (let i = 0; i < typeArray.length; i++) {
            try {
              success = true;
              this.checkType(input, typeArray[i]);
              break;
            } catch (e) {
              success = false;
            }
          }
          if (!success) {
            return this.logError({
              input,
              source,
              valid: false,
              message: `${input} should be of type ${typeArray.map(a => this.getType(a))}`
            });
          }
          return null;
        } else {
          return this.checkArray(input, source);
        }
      } else {
        return this.checkObject(input, source);
      }
    }
    return null;
  },
  checkType(input, source) {
    const t = input ? `${(this.getType(source)[0]).toLowerCase()}${this.getType(source).slice(1)}` : true;
    const tInput = input ? `${(input.constructor.name[0]).toLowerCase()}${input.constructor.name.slice(1)}` : false;
    if (t !== tInput) {
      return this.logError({
        input,
        source,
        valid: false,
        message: `${JSON.stringify(input)} should be of type ${this.getType(source)}`
      });
    }
    return null;
  },
  checkString(input, source) {
    if (source instanceof RegExp) {
      return this.logError({
        input,
        source,
        valid: source.test(input),
      });
    } else {
      return this.logError({
        input,
        source,
        valid: source === input,
      });
    }
  },
  checkNumber(input, source) {
    const minTest = minRegEx.test(source);
    const maxTest = maxRegEx.test(source);
    const rangeTest = rangeRegEx.test(source);
    if (rangeTest && !minTest && !maxTest) {
      const matchMin = source.match(nRegEx)[0];
      const min = parseFloat(matchMin);
      const matchMax = source.match(nRegEx)[1];
      const max = parseFloat(matchMax);
      return this.logError({
        input,
        source,
        valid: input < max && input > min,
      });
    } else if (maxTest && !minTest && !rangeTest) {
      const match = source.match(nRegEx)[0];
      const n = parseFloat(match);
      return this.logError({
        input,
        source,
        valid: input > n,
      });
    } else {
      const match = source.match(nRegEx)[0];
      const n = parseFloat(match);
      return this.logError({
        input,
        source,
        valid: input !== '' && typeof input !== 'undefined' && input !== null && input < n,
      });
    }
  },
  checkArray(input, source) {
    const diffs = difference(source, input);
    return this.logError({
      input: JSON.stringify(input),
      source: JSON.stringify(source),
      valid: diffs.length === 0,
    });
  },
  checkObject(input, source) {
    return this.validate(input, source);
  },
  logError(error) {
    const message = `ValidationError: ${error.input} type:${typeof error.input} must respect ${error.source} type:${typeof error.source}`;
    if (!error.valid) {
      throw new TypeError(error.message || message);
    }
    return null;
  },
  getType(source) {
    return source.name || typeof source;
  }
});

var uuid = () => {
  let d = new Date().getTime();
  if (window.performance && typeof window.performance.now === 'function') {
    d += performance.now(); // use high-precision timer if available
  }
  const uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, (c) => {
    const r = (d + Math.random() * 16) % 16 | 0;
    d = Math.floor(d / 16);
    return (c === 'x'
            ? r
            : (r & 0x3 | 0x8)).toString(16);
  });
  return uuid;
};

const statuses = {
  PENDING: 'PENDING',
  FAILED: 'FAILED',
  RUNNING: 'RUNNING',
  DONE: 'DONE',
};

const schema = {
  action: [Function, Promise],
  '?callback': Function,
  '?params': Array,
  '?meta': Object,
};

/**
 * represents a Process
 * a Process represents an asynchronous or synchronous process to fulfill
 * it contains a notion of status, callback and time to be achieved
 * 
 * @export
 * @class Process
 */
class Process {
  constructor(params) {
    index.validate(params, schema);

    this.id = uuid();
    this.action = params.action;
    this.callback = params.callback;
    this.context = params.context || this;
    this.params = params.params;
    this.meta = params.meta || {};

    this._status = 'PENDING';
    this._time = null;
  }

  /**
   * Set the status of the Process if it is in the statuses enum
   * else throw an error
   * 
   * @memberOf Process
   */
  set status(status) {
    if (statuses[status]) this._status = statuses[status];
    else throw new TypeError(`status: ${status} does not exist`);
  }

  /**
   * Get the status of the process
   * 
   * @readonly
   * 
   * @memberOf Process
   * @return {string} - status
   */
  get status() {
    return this._status;
  }

  /**
   * sets the time of the process using setTime
   *
   * @memberOf Process
   */
  set time(timer) {
    this.setTime(timer);
  }

  /**
   * Get the time elapsed in action since creation of the process
   * 
   * @readonly
   * 
   * @memberOf Process
   * @return {number} - time
   */
  get time() {
    return this._time;
  }

  /**
   * Sets the time elapsed in action since creation of the process
   * 
   * @param {number} [timer=performance.now()] 
   * @returns 
   * 
   * @memberOf Process
   * @return {number} - time
   */
  setTime(timer = performance.now()) {
    return this._time = this._time ? timer - this._time : timer;
  }

  /**
   * custom process resolver for Promise
   * sets the time and bind the callback
   * 
   * @param {function} resolve 
   * @param {any} result 
   * 
   * @memberOf Process
   */
  resolveAction(resolve, result) {
    this.status = 'DONE';
    if (this.callback) this.callback.apply(this.context, this.params);
    this.setTime();
    return resolve(result);
  }

  /**
   * custom process rejecter for Promise
   * sets the time
   * 
   * @param {function} reject 
   * @param {error} e 
   * @returns 
   * 
   * @memberOf Process
   */
  rejectAction(reject, e) {
    console.error(e);
    this.status = 'FAILED';
    this.setTime();
    return reject(e);
  }

  /**
   * execute the Process action
   * bind the context in process.context and args in process.params
   * 
   * @returns {Promise}
   * 
   * @memberOf Process
   */
  execute() {
    this.setTime();
    this.status = 'RUNNING';
    const params = this.params || [];
    return new Promise((resolve, reject) => {
      if (this.action instanceof Promise) {
        return this.action
                .then((result) => this.resolveAction(resolve, result))
                .catch((e) => this.rejectAction(reject, e));
      } else {
        let result = null;
        const action = this.action.bind(this.context, ...params);
        try {
          result = action();
          return this.resolveAction(resolve, result);
        } catch(e) {
          return this.rejectAction(reject, e);
        }
      }
    });
  }
}

module.exports = Process;
