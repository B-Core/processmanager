import Process from './process';

describe('Process class', () => {
  const oConsole = console;
  beforeEach(() => {
    console.error = jest.fn();
  });
  afterEach(() => {
    console = oConsole;
  });

  test('should create a new process', () => {
    const proc = new Process({
      action: () => 'test',
    });

    expect(proc).toBeInstanceOf(Process);
    expect(proc.id).toBeDefined();
    expect(proc.action).toBeDefined();
    expect(proc.callback).not.toBeDefined();
    expect(proc.meta).toBeDefined();
  });
  test('should throw error if action is not Promise or Function', () => {
    expect(() => new Process({
      action: 'test',
    })).toThrow();
  });
  test('should correctly set the status to pending on creation', () => {
    const proc = new Process({
      action: () => 'test',
    });
    expect(proc.status).toEqual('PENDING');
  });
  test('should correctly set the status if it exists', () => {
    const proc = new Process({
      action: () => 'test',
    });

    const goodStatus = [
      'PENDING',
      'FAILED',
      'RUNNING',
      'DONE',
    ];
    
    goodStatus.forEach((g) => {
      proc.status = g;
      expect(proc.status).toEqual(g);
    });

    expect(() => proc.status = 'TEST').toThrow('status: TEST does not exist');
  });

  test('shoud execute action with the right paramaters and as a Promise if action is a simple function', async () => {
    const test = 'test';
    const action = jest.fn((param1) => param1);

    const proc = new Process({
      action,
      params: [test],
    });
    
    const result = await proc.execute();

    expect(action).toHaveBeenCalledWith(proc.params[0]);
    expect(result).toEqual('test');
  });

  test('shoud execute callback if action is resolved', async () => {
    const test = 'test';
    const action = jest.fn((param1) => param1);
    const callback = jest.fn();

    const proc = new Process({
      action,
      params: [test],
      callback,
    });
    
    const result = await proc.execute();

    expect(action).toHaveBeenCalledWith(proc.params[0]);
    expect(callback).toHaveBeenCalledWith('test', proc.params[0]);
    expect(result).toEqual('test');
  });
  test('shoud execute callback if action is a promise and resolved', async () => {
    const test = 'test';
    const action = new Promise((resolve) => resolve(test));
    const callback = jest.fn();

    const proc = new Process({
      action,
      params: [test],
      callback,
    });
    
    const result = await proc.execute();

    expect(callback).toHaveBeenCalledWith(test, proc.params[0]);
    expect(result).toEqual('test');
  });
  test('shoud not execute callback if action is rejected', async () => {
    const test = 'test';
    const action = jest.fn((param1) => param1.test);
    const callback = jest.fn();

    const proc = new Process({
      action,
      callback,
    });
    
    let result = null;

    try {
      result = await proc.execute();
    } catch(e) {} finally {
      expect(action).toHaveBeenCalled();
      expect(callback).not.toHaveBeenCalled();
      expect(console.error).toHaveBeenCalled();
    }
  });
  test('shoud not execute callback if action is a promise and rejected', async () => {
    const test = 'test';
    const action = new Promise((resolve, reject) => reject('error'));
    const callback = jest.fn();

    const proc = new Process({
      action,
      params: [test],
      callback,
    });
    
    let result = null;

    try {
      result = await proc.execute();
    } catch(e) {} finally {
      expect(callback).not.toHaveBeenCalled();
      expect(result).toBeNull();
    }
  });

  test('should set count time on success', async () => {
    var test = 'test';
    const proc = new Process({
      action: new Promise((resolve, reject) => {
        window.setTimeout(() => {
          resolve(test);
        }, 500);
      }),
    });
    
    let result = null;

    try {
      result = await proc.execute();
    } catch(e) {}

    expect(result).toEqual('test');
    expect(proc.time).toBeGreaterThan(490);
    expect(proc.status).toEqual('DONE');
  });
  test('should set count time on fail', async () => {
    var test = 'test';
    const proc = new Process({
      action: new Promise((resolve, reject) => {
        window.setTimeout(() => {
          reject(test);
        }, 500);
      }),
    });
    
    let result = null;

    try {
      result = await proc.execute();
    } catch(e) {}

    expect(result).toEqual(null);
    expect(proc.time).toBeGreaterThan(490);
    expect(proc.status).toEqual('FAILED');
  });
  test('should not have problem if params are undefined and status DONE', async () => {
    const test = 'test';
    const proc = new Process({
      action: new Promise((resolve, reject) => {
        window.setTimeout(() => {
          resolve(test);
        }, 500);
      }),
      callback: jest.fn().mockReturnThis(),
    });
    
    let result = null;
    proc.time = 505;

    try {
      result = await proc.execute();
    } catch(e) {}

    expect(result).toEqual('test');
    expect(proc.time).toBeGreaterThan(1000);
    expect(proc.status).toEqual('DONE');
    expect(proc.callback).toHaveBeenCalledWith(test);
  });
  test('should not have problem if params are undefined and status FAILED', async () => {
    const test = 'test';
    const proc = new Process({
      action: new Promise((resolve, reject) => {
        window.setTimeout(() => {
          reject(test);
        }, 500);
      }),
      callback: jest.fn().mockReturnThis(),
      failCallback: true,
    });
    
    let result = null;
    proc.time = 505;

    try {
      result = await proc.execute();
    } catch(e) {}

    expect(result).toEqual(null);
    expect(proc.time).toBeGreaterThan(1000);
    expect(proc.status).toEqual('FAILED');
    expect(proc.callback).toHaveBeenCalledWith(test);
  });
  test('should set time and compute time if time is already here', async () => {
    const proc = new Process({
      action: new Promise((resolve, reject) => {
        window.setTimeout(() => {
          reject(test);
        }, 500);
      }),
    });
    
    let result = null;
    proc.time = 505;

    try {
      result = await proc.execute();
    } catch(e) {}

    expect(result).toEqual(null);
    expect(proc.time).toBeGreaterThan(1000);
    expect(proc.status).toEqual('FAILED');
  });

  test('should launch callback if failCallback is true', async () => {
    const test = 'test';
    const proc = new Process({
      action: new Promise((resolve, reject) => {
        window.setTimeout(() => {
          reject(test);
        }, 500);
      }),
      callback: jest.fn().mockReturnThis(),
      failCallback: true,
      params: ['test'],
    });
    
    let result = null;
    proc.time = 505;

    try {
      result = await proc.execute();
    } catch(e) {}

    expect(result).toEqual(null);
    expect(proc.time).toBeGreaterThan(1000);
    expect(proc.status).toEqual('FAILED');
    expect(proc.callback).toHaveBeenCalledWith(test, 'test');
  });
});