import check from '../../check/dist/index';
import uuid from '../../microutils/dist/lang/uuid';


const statuses = {
  PENDING: 'PENDING',
  FAILED: 'FAILED',
  RUNNING: 'RUNNING',
  DONE: 'DONE',
}

const schema = {
  action: [Function, Promise],
  '?callback': Function,
  '?params': Array,
  '?meta': Object,
  '?failCallback': Boolean,
};

/**
 * represents a Process
 * a Process represents an asynchronous or synchronous process to fulfill
 * it contains a notion of status, callback and time to be achieved
 * 
 * @export
 * @class Process
 */
export default class Process {
  constructor(params) {
    check.validate(params, schema);

    this.id = uuid();
    this.action = params.action;
    this.callback = params.callback;
    this.context = params.context || this;
    this.params = params.params;
    this.meta = params.meta || {};
    this.failCallback = params.failCallback || false;

    this._status = 'PENDING';
    this._time = null;
  }

  /**
   * Set the status of the Process if it is in the statuses enum
   * else throw an error
   * 
   * @memberOf Process
   */
  set status(status) {
    if (statuses[status]) this._status = statuses[status];
    else throw new TypeError(`status: ${status} does not exist`);
  }

  /**
   * Get the status of the process
   * 
   * @readonly
   * 
   * @memberOf Process
   * @return {string} - status
   */
  get status() {
    return this._status;
  }

  /**
   * sets the time of the process using setTime
   *
   * @memberOf Process
   */
  set time(timer) {
    this.setTime(timer);
  }

  /**
   * Get the time elapsed in action since creation of the process
   * 
   * @readonly
   * 
   * @memberOf Process
   * @return {number} - time
   */
  get time() {
    return this._time;
  }

  /**
   * Sets the time elapsed in action since creation of the process
   * 
   * @param {number} [timer=performance.now()] 
   * @returns 
   * 
   * @memberOf Process
   * @return {number} - time
   */
  setTime(timer = performance.now()) {
    return this._time = this._time ? timer - this._time : timer;
  }

  /**
   * custom process resolver for Promise
   * sets the time and bind the callback
   * 
   * @param {function} resolve 
   * @param {any} result 
   * 
   * @memberOf Process
   */
  resolveAction(resolve, result) {
    this.status = 'DONE';
    if (this.callback) this.callback.apply(this.context, [result, ...(this.params ? this.params : [])]);
    this.setTime();
    return resolve(result);
  }

  /**
   * custom process rejecter for Promise
   * sets the time
   * 
   * @param {function} reject 
   * @param {error} e 
   * @returns 
   * 
   * @memberOf Process
   */
  rejectAction(reject, e) {
    console.error(e);
    this.status = 'FAILED';
    if (this.failCallback && this.callback) this.callback.apply(this.context, [e, ...(this.params ? this.params : [])]);
    this.setTime();
    return reject(e);
  }

  /**
   * execute the Process action
   * bind the context in process.context and args in process.params
   * 
   * @returns {Promise}
   * 
   * @memberOf Process
   */
  execute() {
    this.setTime();
    this.status = 'RUNNING';
    const params = this.params || [];
    return new Promise((resolve, reject) => {
      if (this.action instanceof Promise) {
        return this.action
                .then((result) => this.resolveAction(resolve, result))
                .catch((e) => this.rejectAction(reject, e));
      } else {
        let result = null;
        const action = this.action.bind(this.context, ...params);
        try {
          result = action();
          return this.resolveAction(resolve, result);
        } catch(e) {
          return this.rejectAction(reject, e);
        }
      }
    });
  }
}