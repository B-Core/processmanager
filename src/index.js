/*
   Copyright 2018 Ciro DE CARO

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

/**
 * reprensents a ProcessManager
 * It handles process execution and fails
 * keep tracks of processes fail and success status
 * 
 * @export
 * @class ProcessManager
 */
export default class ProcessManager {
  constructor() {
    this._queue = [];
    this._failedQueue = [];
    this._successQueue = [];
  }

  /**
   * Add a process to the ProcessManager queue
   * 
   * @param {Process} proc 
   * 
   * @memberOf ProcessManager
   */
  add(proc) {
    const found = this._queue.find(p => p.id === proc.id);
    if (!found) this._queue.push(proc);
    else throw new Error(`process 'id: ${found.id}' already exists`);
  }

  /**
   * Remove a process form the ProcessManager queue
   * 
   * @param {string} id 
   * 
   * @memberOf ProcessManager
   */
  remove(id) {
    const index = this._queue.findIndex(p => p.id === id);
    if (index >= 0) this._queue.splice(index, 1);
    else throw new Error(`process 'id: ${id}' doesn't exist`);
  }

  /**
   * ProcessManager general resolver
   * 
   * @param {function} resolve 
   * @param {Process} proc 
   * @param {any} result 
   * @returns 
   * 
   * @memberOf ProcessManager
   */
  resolveProcess(resolve, proc, result) {
    this._successQueue.push(proc);
    return resolve(result);
  }

  /**
   * ProcessManager general rejecter
   * 
   * @param {function} reject 
   * @param {Process} proc 
   * @param {error} error 
   * @returns 
   * 
   * @memberOf ProcessManager
   */
  rejectProcess(reject, proc, error) {
    console.error(error);
    this._failedQueue.push(proc);
    return reject(error);
  }

  executeAllProcess(final, resolve, result) {
    if (result) final.push(result);
    if (this._queue.length > 0) {
      this.execute()
          .then((r) => this.executeAllProcess(final, resolve, r))
          .catch((r) => this.executeAllProcess(final, resolve, r));
    } else {
      return resolve(final);
    }
  }

  /**
   * execute all process in the queue sequentially
   * 
   * @returns {promise} - promise
   * 
   * @memberOf ProcessManager
   */
  executeAll() {
    return new Promise((resolve) => {
      const final = [];
      this.executeAllProcess(final, resolve);
    });
  }

  /**
   * execute the first process in the queue
   * 
   * @returns {promise} - promise
   * 
   * @memberOf ProcessManager
   */
  execute() {
    return new Promise((resolve, reject) => {
      const proc = this.next;
      if (proc) {
        return proc.execute()
              .then((result) => this.resolveProcess(resolve, proc, result))
              .catch((error) => this.rejectProcess(reject, proc, error));
      } else {
        console.log('no processes left in queue');
        return resolve();
      }
    });
  }

  /**
   * get the next process in the queue
   * 
   * @readonly
   * 
   * @memberOf ProcessManager
   */
  get next() {
    return this._queue.length > 0 ? this._queue.shift() : null;
  }

  /**
   * get the sum of all processes time
   * 
   * @readonly
   * 
   * @memberOf ProcessManager
   */
  get totalTime() {
    return this._successQueue.concat(this._failedQueue).reduce((acc, cv) => acc += parseFloat(cv.time), 0);
  }
}