const path = require('path');

const defaultConfig = require('./webpack.default.config');

const baseConfig = {
  ...defaultConfig,
  mode: process.env.NODE_ENV || "production",
  target: "web",
  entry: {
    process: path.resolve('src/models/process.js'),
  },
  output: {
    path: path.resolve('dist/web/models'),
    filename: '[name].js',
  },
};

const umdConfig = {
  ...baseConfig,
  output: {
    ...baseConfig.output,
    filename: '[name].umd.js',
    libraryTarget: 'umd',
  }
};

const commonConfig = {
  ...baseConfig,
  output: {
    ...baseConfig.output,
    filename: '[name].commonjs.js',
    libraryTarget: 'commonjs',
  }
};

const configs = [
  baseConfig,
  umdConfig,
  commonConfig,
]

const nodeConfigs = configs.map(c => ({
  ...c,
  target: "node",
  output: {
    ...c.output,
    path: path.resolve('dist/node/models')
  }
}));

module.exports = [
  ...configs,
  ...nodeConfigs,
];
