import Process from './models/process';
import ProcessManager from './index';

describe('ProcessManager class', () => {
  const oConsole = console;
  let manager = null;

  beforeEach(() => {
    manager = new ProcessManager();
    console.error = jest.fn();
    console.log = jest.fn();
  });

  afterEach(() => {
    manager = null;
    console = oConsole;
  });
  
  test('should correctly init a ProcessManager', () => {
    expect(manager).toBeInstanceOf(ProcessManager);
    expect(manager._failedQueue).toBeDefined();
    expect(manager._successQueue).toBeDefined();
  });

  test('should add a proc if it doesn\'t exists', () => {
    const action = jest.fn();
    const proc = new Process({
      action,
    });

    manager.add(proc);

    expect(manager._queue.find(p => p.id === proc.id)).toEqual(proc);

    expect(() => manager.add(proc)).toThrow(`process 'id: ${proc.id}' already exists`);
  });

  test('should remmove a proc if it exists', () => {
    const action = jest.fn();
    const proc = new Process({
      action,
    });

    expect(() => manager.remove(proc.id)).toThrow(`process 'id: ${proc.id}' doesn't exist`);

    manager.add(proc);
    manager.remove(proc.id);

    expect(manager._queue.find(p => p.id === proc.id)).toEqual(undefined);
  });

  test('should resolve next process and add it to the successQueue if success', async () => {
    const test = 'test';
    const action = jest.fn((param1) => param1);
    const proc = new Process({
      action,
      params: [test],
    });

    manager.add(proc);
    
    let result = null;

    try {
      result = await manager.execute();
    } catch(e) {}

    expect(action).toHaveBeenCalledWith(test);
    expect(result).toEqual(test);
    expect(manager._successQueue.find(p => p.id === proc.id)).toEqual(proc);
    expect(manager._queue).toHaveLength(0);
  });

  test('should reject next process and add it to the failQueue if fail', async () => {
    const action = new Promise((resolve, reject) => reject());
    const proc = new Process({
      action,
    });

    manager.add(proc);
    
    let result = null;

    try {
      result = await manager.execute();
    } catch(e) {} finally {
      expect(result).toEqual(null);
      expect(manager._failedQueue.find(p => p.id === proc.id)).toEqual(proc);
      expect(manager._queue).toHaveLength(0);
    }
  });
  
  test('should log that no process are left if there isn\'t any', () => {
    manager.execute();
    expect(console.log).toHaveBeenCalledWith('no processes left in queue');
  });

  test('should resolve all processes and compute totalTime', async () => {
    const test = 'test';
    const callback = jest.fn(function (param) {
      return param;
    });
    const callback2 = jest.fn(function (param2) {
      return param2;
    });
    const callback3 = jest.fn(function (param3) {
      return param3;
    });
    const proc = new Process({
      action: new Promise((resolve, reject) => {
        window.setTimeout(() => {
          return resolve(test);
        }, 1000);
      }),
      callback,
      params: [test],
    });
    const proc2 = new Process({
      action: new Promise((resolve, reject) => {
        window.setTimeout(() => {
          return reject('error');
        }, 1000);
      }),
      callback: callback2,
    });
    const proc3 = new Process({
      action: new Promise((resolve, reject) => {
        window.setTimeout(() => {
          return resolve(test);
        }, 1000);
      }),
      callback: callback3,
    });

    manager.add(proc);
    manager.add(proc2);
    manager.add(proc3);

    manager.executeAll().then((result) => {
      expect(manager._queue).toHaveLength(0);
    
      expect(callback).toHaveBeenCalledWith(test);
      expect(callback2).not.toHaveBeenCalled();
      expect(callback3).toHaveBeenCalled();
      
      expect(manager.totalTime).toBeGreaterThan(2990);
      expect(result).toEqual(['test', 'error', 'test']);
    })
  });

  test('should not reject if one promise fails', async () => {
    const proc = new Process({
      action: new Promise((resolve, reject) => reject('error')),
    });
    const proc2 = new Process({
      action: new Promise((resolve, reject) => {
        window.setTimeout(() => {
          resolve('test');
        }, 205);
      }),
    });

    manager.add(proc);
    manager.add(proc2);
    
    let result = null;

    try {
      result = await manager.executeAll();
    } catch(e) {}

    expect(manager._failedQueue).toHaveLength(1);
    expect(manager._queue).toHaveLength(0);
    expect(manager.totalTime).toBeGreaterThan(200);
    expect(result).toEqual(['error', 'test']);
  });
});