const webpack = require('webpack');
const path = require('path');

const defaultConfig = require('./webpack.default.config');
const processConfig = require('./webpack.config.models');

const baseConfig = {
  ...defaultConfig,
  mode: process.env.NODE_ENV || "production",
  entry: {
    index: path.resolve('src/index.js'),
  },
  output: {
    path: path.resolve('dist/web'),
    filename: '[name].js',
    libraryTarget: 'module',
  },
};

const umdConfig = {
  ...baseConfig,
  output: {
    ...baseConfig.output,
    filename: '[name].umd.js',
    libraryTarget: 'umd',
  }
};

const commonConfig = {
  ...baseConfig,
  output: {
    ...baseConfig.output,
    filename: '[name].commonjs.js',
    libraryTarget: 'commonjs',
  }
};

const configs = [
  baseConfig,
  umdConfig,
  commonConfig,
];

const nodeConfigs = configs.map(c => ({
  ...c,
  target: "node",
  output: {
    ...c.output,
    path: path.resolve('dist/node')
  }
}));

module.exports = [
  ...configs,
  ...nodeConfigs,
  ...processConfig,
];
